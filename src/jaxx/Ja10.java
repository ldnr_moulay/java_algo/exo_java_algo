/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        double a=0;
        double b=0;
        double c=0;
     
        System.out.print("Saisissez a : ");
        a = scanner.nextDouble();
        System.out.println("a : " + a);
        
        System.out.print("Saisissez b : ");
        b = scanner.nextDouble();
        System.out.println("b : " + b);
        
        System.out.print("Saisissez c : ");
        c = scanner.nextDouble();
        System.out.println("c : " + c);
        
        double delta = (Math.pow(b, 2))-(4*a*c);
        double x1= (-b+Math.sqrt(delta))/(2*a);
        double x2= (-b-Math.sqrt(delta))/(2*a);
        double x= -b/(2*a);
        
        System.out.println("delta : " + delta);
        
        if(delta > 0){
            System.out.println("Delta > 0, il y a deux solutions"+
                    " "+"x1 :"+" "+ x1 +" "+ "et"+" "+"x2 :"+" " + x2 );
        }
        if(delta == 0){
            System.out.println("Delta = 0, il y a une solution"+" "
                    +"x :"+" "+ x);
        }
         
        if(delta < 0){
            System.out.println("Delta < 0, il n'y a pas de solutions");
        }
    }
    
}
