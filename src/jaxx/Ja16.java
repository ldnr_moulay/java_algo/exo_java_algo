/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja16 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int day = 0;

        System.out.println("Numero du jour ? ");
        day = scanner.nextInt();
        System.out.println(day);
        LocalDate numOfDays = Year.now(ZoneId.of("Europe/Paris")).atDay(day);
        int numJourSemaine = cal.get(day);
        String jourSemaine = "";
        String message= "Le  " + " " + day + " " + "correspond au " + " "
                        + numOfDays + " " + "et est un " + " ";
        switch (numJourSemaine) {
            case 1:
                jourSemaine = "Lundi";
                System.out.println(message+jourSemaine);
                break;
            case 2:
                jourSemaine = "Mardi";
                System.out.println(message+jourSemaine);
                break;
            case 3:
                jourSemaine = "Mercredi";
                System.out.println(message+jourSemaine);
                break;
            case 4:
                jourSemaine = "Jeudi";
                System.out.println(message+jourSemaine);
                break;
            case 5:
                jourSemaine = "Vendredi";
                System.out.println(message+jourSemaine);
                break;
            case 6:
                jourSemaine = "Samedi";
                System.out.println(message+jourSemaine);
                break;
            case 7:
                jourSemaine = "Dimanche";
                System.out.println(message+jourSemaine);
                break;
            default:
                break;
        }

    }

}
