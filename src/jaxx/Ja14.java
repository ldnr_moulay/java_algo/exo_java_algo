/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        Double note;
        int i = 0;
        int somme = 0;
        int nbNote=0;
        int moyenne=0;
        double min=0;

        do {
            System.out.print("Note ? ");
            note = scanner.nextDouble();
            System.out.println(note);
            if (note >= 0) {
                somme += note;
                nbNote+=1;
                moyenne=somme/nbNote;
            }
            System.out.println("somme des notes:" + " " + somme);
            System.out.println("Nombre de notes:" + " " + nbNote);
            System.out.println("Moyenne des notes:" + " " + moyenne);
            i++;
        } while (note >= 0);
        min=Math.min(note, note);
        System.out.println("note minimum :"+min);

    }

}
