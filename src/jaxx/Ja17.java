/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        int donnee = 0;

        System.out.println("Entrez les Minutes ? ");
        donnee = scanner.nextInt();
        System.out.println(donnee);

        int heu = donnee / 60;
        int min = donnee % 60;

        System.out.println(donnee + " minutes vaut " + heu + "h" + min);

    }

}
