/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

/**
 *
 * @author stag
 */
public interface Ja50 {
    
    /*Le  but   de  l'exercice est  de  définir les  signatures des   fonctions ci-dessous.
Vous devez définir pour    chaque fonction, sa signature, c’est   à dire:•son nom•
le  ou  les  paramètres en  entrée ainsi    que   les  types associés•le  type  du paramètre en  sortie. 
S’il   n’y  en  a pas,   utilisez «void»*/

/*Fonction1: elle   calcule la  parité d’un   nombre quelconque en  retournant «pair» ouimpair»*/

String parite(int nb);

//Fonction 2: elle   calcule la  parité d’un   nombre quelconque en  affichant «pair» ou  impair».

void pariteVoid(int nb);

//Fonction 3: elle  réalise une   conversion d’euros en  dollars et  retourne le  montant en  dollars
    
double euroDollar (int dollar);

//Fonction 4: elle  retourne la  tangente d’un   angle

double tangente (double angle);

//Fonction 5: elle   retourne la  surface d’un   cercle

double surfaceCercle (int rayon);

//Fonction 6: elle  affiche la  table    de  multiplication d’un   entier compris entre 1 et  10

double tableMultiplication (int nb);

//Fonction 7: elle  retourne un  nombre aléatoire compris entre deux valeurs

double nbAleatoire (double min, double max);

//Fonction 8: elle   affiche un  rectangle de  dimensions L x H.

String afficheRectangle (int largeur, int hauteur);
        
        


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
