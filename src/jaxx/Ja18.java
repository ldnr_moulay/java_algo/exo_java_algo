/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        double nb1 = 0;
        String op = "";
        double nb2 = 0;
        double result = 0;

        System.out.println("Saisissez le premier nombre ? ");
        nb1 = scanner.nextInt();

        System.out.println("Saisissez l'opérateur ? ");
        op = scanner.next();

        System.out.println("Saisissez le deuxieme nombre ? ");
        nb2 = scanner.nextInt();

        if (op == null) {
            System.out.println("Entrez un opérateur ou un nombre correct");
        } else {
            switch (op) {
                case "/":
                    if (nb2 == 0) {
                        do {
                            System.out.println("Division par 0 impossible");
                            System.out.println("Saisissez le deuxieme nombre ? ");
                            nb2 = scanner.nextInt();
                        } while (nb2 == 0);
                    }
                    result = nb1 / nb2;
                    System.out.println("resultat = " + " " + result);
                    break;
                case "x":
                    result = nb1 * nb2;
                    System.out.println("resultat = " + " " + result);
                    break;
                case "-":
                    result = nb1 - nb2;
                    System.out.println("resultat = " + " " + result);
                    break;
                case "+":
                    result = nb1 + nb2;
                    System.out.println("resultat = " + " " + result);
                    break;
                default:
                    System.out.println("Entrez un opérateur ou un nombre correct");
                    break;
            }
        }
    }
}
