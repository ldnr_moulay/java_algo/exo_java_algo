/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja08 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       scanner.useLocale(Locale.ENGLISH); 
       double moyenne=0;
       System.out.print( "Saisissez la moyenne au bac : " );
            moyenne = scanner.nextDouble();

       if(moyenne>=16){
           System.out.println("Felicitations mention très bien");
       }
       else if(moyenne>=14 && moyenne<16){
           System.out.println("Super mention bien");
       }
       else if(moyenne>=12 && moyenne<14){
           System.out.println("Bravo mention assez bien");
       }
       else {
           System.out.println("Arf dommage pas de mention");
       }
           
       }
    }
    
