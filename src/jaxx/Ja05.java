/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja05 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH); 
        System.out.print( "Saisissez un nombre : ");
        double nombre = scanner.nextDouble();
        System.out.println("Le nombre que vous avez tapé est : "+ nombre);
        double reste = nombre %2;
        if(nombre %2 == 0 ){
            System.out.println("Le nombre est pair, le reste est de "+ reste);
        }
        else{
            System.out.println("Le nombre n'est pas pair, le reste est de "+ reste);
        }
    }
    
}
