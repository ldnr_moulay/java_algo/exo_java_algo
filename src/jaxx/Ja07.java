/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Scanner;
import java.util.Locale;

/**
 *
 * @author stag
 */
public class Ja07 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       scanner.useLocale(Locale.ENGLISH); 
       double temp=0;
       System.out.print( "Saisissez la temperature : " );
            temp = scanner.nextDouble();

       if(temp<0){
           System.out.println("L'état de l'eau est solide");
       }
       else if(temp>=0 && temp<100){
           System.out.println("L'état de l'eau est liquide");
       }
       else if(temp>=100){
           System.out.println("L'état de l'eau est gazeux");
       }
           
       }
     
}
