/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja13b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        
        int largeur = 0;
        int hauteur = 0;


        System.out.println("Largeur ?");
        largeur = clavier.nextInt();

        System.out.println("Hauteur ?");
        hauteur = clavier.nextInt();


        System.out.println("-- Rectangle " + largeur + "x" + hauteur + " --");
        for (int i = 1; i <= hauteur; i++) {
            for (int j = 1; j <= largeur; j++) {
                if (j == 1 || j == largeur || i == 1 || i == hauteur) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
       
    }
    
}
