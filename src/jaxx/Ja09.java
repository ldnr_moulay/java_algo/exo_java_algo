/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja09 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        double taille = 0;
        double masse = 0;
        double indice = 0;

        System.out.print("Saisissez votre taille en mètre : ");
        taille = scanner.nextDouble();
        System.out.println("Taille : " + taille);

        System.out.print("Saisissez votre masse en kg: ");
        masse = scanner.nextDouble();
        System.out.println("Masse : " + masse);
        indice = masse / Math.pow(taille, 2);

        if (indice < 16.5) {
            System.out.println("imc :" + indice + ":"+"dénutrition ou famine");
        }
        if (indice >= 16.5 && indice < 18.5) {
            System.out.println("imc :" + indice + ":"+"maigreur");
        }
        if (indice >= 18.5 && indice < 25) {
            System.out.println("imc :" + indice + ":"+"corpulence normal");
        }
        if (indice >= 25 && indice < 30) {
            System.out.println("imc :" + indice + ":"+"surpoids");
        }
        if (indice >= 30 && indice < 35) {
            System.out.println("imc :" + indice + ":"+"obésité modérée");
        }
        if (indice >= 35 && indice <= 40) {
            System.out.println("imc :" + indice + ":"+"obésité sévère");
        }
        if (indice > 40) {
            System.out.println("imc :" + indice + ":"+"obésité morbide ou massive");
        }
    }
}
