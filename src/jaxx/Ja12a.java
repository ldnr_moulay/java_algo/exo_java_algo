/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja12a {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        int nombre, i;

        System.out.print("Saisissez le nombre dont vous voulez la table : ");
        nombre = scanner.nextInt();
        System.out.println("La table de multiplication de " + nombre);

        for (i = 1; i <= 10; i++) {
            System.out.println(nombre + "x" + i + " = " + (nombre * i));
        }
    }

}
