/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxx;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author stag
 */
public class Ja11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.ENGLISH);

        int nbCote;
        String figure = "";

        System.out.print("Saisissez le nombre de côté : ");
        nbCote = scanner.nextInt();
        System.out.println("Nombre côté : " + nbCote);
        switch (nbCote) {
            case 3:
                figure="Triangle";
                break;
            case 4:
                figure="Quadrilatère";
                break;
            case 5:
                figure="Pentagone";
                break;
            case 6:
                figure="Hexagone";
                break;
            case 8:
                figure="Octogone";
                break;
            case 12:
                figure="Dodecagone";
                break;
            default:
                figure="Je ne sais pas";
                break;
        }
        System.out.println("Un polygone à" + " " + nbCote + " " + "côtés" + " "
                    + "est un "+" "+ figure);

    }

}
